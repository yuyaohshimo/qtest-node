import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import config from './config';
import api from './api';
import initializeDb from './db';

let app = express();

app.set('view engine', 'jade');
app.set('views', __dirname + '/views');

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// connect to db
initializeDb((err, db) => {

  // api router
  app.use('/api', api({ config, db }));

  // assume 404 since no middleware responded
  app.use(function(req, res) {
    res.status(404).render('404', { url: req.originalUrl });
  });

  app.listen(config.port);
});

export default app;
