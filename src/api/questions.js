import { Router } from 'express';

export default ({ config, db }) => {
  let api = Router();

  api.get('/', (req, res) => {
    db.query('SELECT * from questions', (err, rows, fields) => {
      if (err) {
        return res.json(500, { error: err.code });
      }
      res.json(rows);
    });
  });

  api.get('/:id', (req, res) => {
    db.query(`SELECT * from questions WHERE id=${req.params.id}`, (err, rows, fields) => {
      if (err) {
        return res.json(500, { error: err.code });
      }
      res.json(rows[0]);
    });
  });

  api.post('/', (req, res) => {
    db.query('INSERT INTO questions SET ?', req.body, (err, result) => {
      if (err) {
        return res.json(500, { error: err.code });
      }
      res.json(201, Object.assign(req.body, { id: result.insertId }));
    });
  });

  api.patch('/:id', (req, res) => {
    db.query('UPDATE questions SET content=?, answer=? WHERE id=?', [req.body.content, req.body.answer, req.params.id], function(err, result) {
      if (err) {
        return res.json(500, { error: err.code });
      }
      res.json(200, Object.assign(req.body, { id: Number(req.params.id) }));
    });
  });

  api.delete('/:id', (req, res) => {
    const id = req.params.id;
    db.query(`DELETE FROM questions WHERE id=${id}`, function (err, result) {
      if (err) {
        return res.json(500, { error: err.code });
      }
      res.send(200);
    });
  });

  return api;
};
