import { Router } from 'express';
import questions from './questions';

export default ({ config, db }) => {

  let api = Router();

  api.use('/questions', questions({ config, db }));

  return api;
};
