import chai from 'chai';
import chaiHttp from 'chai-http';

const should = chai.should();
chai.use(chaiHttp);

import server from '../';

import db from '../db';

describe('Questions', () =>  {

  beforeEach(done => {
    db().query('TRUNCATE TABLE questions', () => {
      db().query('INSERT INTO questions SET ?', {
        content: 'How many vowels are there in the English alphabet?',
        answer: 'five'
      }, () => {
        done();
      });
    });
  });

  afterEach(done => {
    db().query('TRUNCATE TABLE questions', () => {
      done();
    });
  });

  it('should add a single question on /api/questions POST', done => {
    chai.request(server)
      .post('/api/questions')
      .send({ content: 'content', answer: 'five' })
      .end((err, res) => {
        res.should.have.status(201);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('id');
        res.body.should.have.property('content');
        res.body.should.have.property('answer');
        res.body.id.should.equal(2);
        res.body.content.should.equal('content');
        res.body.answer.should.equal('five');
        done();
      });
  });

  it('should list all questions on /api/questions GET', done => {
    chai.request(server)
      .get('/api/questions')
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.be.a('array');
        res.body[0].should.have.property('id');
        res.body[0].should.have.property('content');
        res.body[0].should.have.property('answer');
        done();
      });
  });

  it('should list a single question on /api/questions/:id GET', done => {
    chai.request(server)
      .get('/api/questions/1')
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('id');
        res.body.should.have.property('content');
        res.body.should.have.property('answer');
        res.body.id.should.equal(1);
        done();
      });
  });

  it('should update a single question on /api/questions/:id PUT', done => {
    chai.request(server)
      .patch('/api/questions/1')
      .send({ content: 'content', answer: 'six' })
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.be.a('object');
        res.body.should.have.property('id');
        res.body.should.have.property('content');
        res.body.should.have.property('answer');
        res.body.id.should.equal(1);
        res.body.content.should.equal('content');
        res.body.answer.should.equal('six');
        done();
      });
  });

  it('should delete a single question on /api/questions/:id DELETE', done => {
    chai.request(server)
      .delete('/api/questions/1')
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

});
