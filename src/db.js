import config from './database';
import mysql from 'mysql';
const env = (process.env.NODE_ENV === 'test') ? 'test' : 'production';
let connection = mysql.createConnection({
  host: config[env].host,
  user: config[env].user,
  password: config[env].password,
  database: config[env].database
});
let db;

export default callback => {
  if (db) {
    return db;
  }
  connection.connect(err => {
    if (err) {
      return callback(err);
    }
    db = connection;
    callback(null, connection);
  });
};
