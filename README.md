# qtest-node

## Development

```
npm run dev
```

## Test

1. Create database named `qtest_test`

2. Add table

```
npm run db:migrate:up:test
```

3. Run test

```
npm run test
```

## Run Server

1. Create database named `qtest`

2. Add table

```
npm run db:migrate:up:production
```

3. Start server

```
npm start
```

## API

- `GET /api/questions`
- `GET /api/questions/:id`
- `POST /api/questions`
- `PATCH /api/questions/:id`
- `DELETE /api/questions/:id`
